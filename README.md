# Campaigns

## Need

1. Tie domain name to website.
2. Decide the host
3. Create gitlab project
4. Configure DNS
5. Set up Marketing google workspace account
6. Set up email for beyond_benefits, tie to workspace. add secondary domain.
7. DBA name for the marketing "agency", use as main account for workspace.
8. Set up seperate company for marketing analytics, beyond benefits
9. Set up facebook biz manager account for TF marketing.
10. Add headline and body copy for BB.
11. add privacy and terms to page.
