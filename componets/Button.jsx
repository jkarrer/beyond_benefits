export default function Button() {
  return (
    <>
      <button id="conversion-button">
        <a id="phone-number" href="tel:855-339-1361">
          Claim Your Device
        </a>
      </button>

      <a href="tel:855-339-1361" className="button__subtext">
        Or call toll-free (888) 339-1361
      </a>
      <style jsx>{`
        button {
          display: block;
          background-color: var(--secondary);

          width: 100%;
          height: 68px;
          margin: auto;

          border: none;
          border-radius: 14px;

          box-shadow: 1px 6px 10px 2px rgba(86, 91, 230, 0.6);

          font-size: 1.5rem;
          font-weight: 700;
          color: white;
          text-align: center;
        }
        .button__subtext {
          display: block;

          text-decoration: none;
          color: var(--secondary);
          font-size: calc(0.5rem + 2.5vw);
          font-weight: 600;
          text-align: center;
          line-height: 1.4;
          padding-top: 24px;
        }
        @media (min-width: 400px) {
          .button__subtext {
            font-size: 1.2rem;
          }
        }
      `}</style>
    </>
  );
}
