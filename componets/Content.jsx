export default function Content() {
  return (
    <>
      <p class="content">
        Tap below before this reservation time ends to claim this free*
        life-saving device used by hundreds of thousands of seniors across
        North&nbsp;America.
      </p>
      <style jsx>{`
        .content {
          margin: 16px auto 32px auto;

          font-size: calc(0.5rem + 2.5vw);
          font-weight: 500;
          text-align: center;
          line-height: 1.3;
        }
        @media (min-width: 400px) {
          .content {
            font-size: 1.2rem;
          }
        }
      `}</style>
    </>
  );
}
