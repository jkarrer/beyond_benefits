export default function Header() {
  return (
    <>
      <header>
        <a href="/">
          <img src="/beyond_benefits_typeface.png" alt="beyond benefits logo" />
        </a>
      </header>
      <style jsx>{`
        header {
          padding: 8px 18px;
          border-bottom: #b8b8b8 solid 1px;
        }
        header img {
          display: block;
          width: 40vw;
          max-width: 160px;
        }
        @media (min-width: 600px) {
          header img {
            margin: auto;
          }
        }
      `}</style>
    </>
  );
}
