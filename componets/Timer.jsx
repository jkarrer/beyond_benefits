import { useEffect, useState } from "react";

export default function Timer() {
  const [minutes, setMinutes] = useState(4);
  const [seconds, setSeconds] = useState(59);

  useEffect(() => {
    let myInterval = setInterval(() => {
      if (seconds > 0) {
        setSeconds(seconds - 1);
      }
      if (seconds === 0) {
        if (minutes === 0) {
          clearInterval(myInterval);
        } else {
          setMinutes(minutes - 1);
          setSeconds(59);
        }
      }
    }, 1000);
    return () => {
      clearInterval(myInterval);
    };
  });

  return (
    <>
      <div class="timer__container">
        <p class="timer__subtext">Your device has been reserved for:</p>
        <div class="timer__countdown" id="countdown">
          {minutes === 0
            ? "00:00"
            : `0${minutes}:${seconds < 10 ? `0${seconds}` : `${seconds}`}`}
        </div>
      </div>

      <style jsx>{`
        .timer__container {
          color: rgb(46, 159, 46);
          margin: auto;

          text-align: center;
        }
        .timer__countdown {
          font-size: calc(1rem + 18vw);
          font-weight: 700;
        }
        .timer__subtext {
          font-size: 18px;
          font-weight: 500;
        }
        @media (min-width: 400px) {
          .timer__countdown {
            font-size: 5.5rem;
          }
        }
      `}</style>
    </>
  );
}
