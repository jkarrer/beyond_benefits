export default function Footer() {
  return (
    <>
      <footer>
        <div class="links__container">
          <a class="links__link" href="/privacy">
            Privacy Policy
          </a>
          <h1 class="links__line">|</h1>
          <a class="links__link" href="/terms">
            Terms of Service
          </a>
        </div>
        <p class="copyright">© Copyright BeyondBenefits.co 2022</p>
        <p class="disclaimer">
          * Free device with enrollment into our 24/7 monitoring program.
        </p>
      </footer>
      <style jsx>{`
        footer {
          margin-top: 30%;
          padding-bottom: 16px;
        }

        footer .links__container {
          display: flex;
          align-items: center;
          justify-content: space-between;

          width: 240px;
          margin: auto;
          padding-bottom: 24px;
        }

        footer .links__link {
          font-size: 0.9rem;
          line-height: 0;
          font-weight: 600;
          color: var(--secondary);
          text-decoration: none;
        }

        footer .links__line {
          font-size: 1.6rem;
          line-height: 0;
          font-weight: 400;
          color: var(--secondary);
        }

        footer .copyright {
          font-size: 14px;
          text-align: center;
          font-weight: 500;

          padding-bottom: 52px;
        }

        footer .disclaimer {
          font-size: 14px;
          text-align: center;
          font-weight: 500;

          width: 260px;
          margin: auto;
        }
        @media (min-width: 600px) {
          footer {
            position: absolute;
            width: 400px;

            bottom: 10px;
            left: 0;
            right: 0;
            margin: auto;
          }
          footer .copyright {
            padding-bottom: 32px;
          }
        }
      `}</style>
    </>
  );
}
