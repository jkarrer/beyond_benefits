export default function Title() {
  return (
    <>
      <h1 class="title">
        <strong>You've qualified!</strong>
        <br />A Medical Alert Device has just been reserved for
        you&nbsp;at&nbsp;
        <u>no&nbsp;cost</u>.
      </h1>

      <style jsx>{`
        .title {
          margin: 0 auto 24px auto;

          font-size: calc(0.7rem + 5.2vw);
          font-weight: 500;
          line-height: 1.2;

          text-align: center;
        }
        @media (min-width: 400px) {
          .title {
            font-size: 2.1rem;
          }
        }
      `}</style>
    </>
  );
}
