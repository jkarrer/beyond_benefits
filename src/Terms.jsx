import Header from "../componets/Header";

export default function Terms() {
  return (
    <>
      <Header />

      <div class="terms">
        <p>
          <h3>Terms</h3>
        </p>

        <p>
          This website (the “Website”) is operated by Life Protect 24/7, Inc.
          (“Life Protect 24/7”, “we”, “us” or “our”). It is intended for your
          information, education and communication. Please feel free to browse
          the Website; however, your access and use of the Website is subject to
          the following terms and conditions (“Terms of Use”) and all applicable
          laws. By accessing the Website, you accept, without limitation or
          qualification, these Terms of Use.
        </p>
        <p>
          Life Protect 24/7 reserves the right, at its sole discretion, to
          change or modify these Terms of Use at any time. Changes and/or
          modifications shall become effective immediately upon posting. If we
          make any changes to these Terms of Use, we will change the “Last
          Updated” date above. Please review these Terms of Use periodically.
          Your use of the Website will be governed by the Terms of Use in place
          on the date you access the Website, and your use of the Website will
          constitute acceptance of those Terms of Use.
        </p>
        <p>
          Please note that these Terms of Use only apply to the use of the
          Website. A separate set of Terms and Conditions apply to the use of
          any Life Protect 24/7 devices and services. This separate set of Terms
          and Conditions is provided to all customers and is available upon
          request.
        </p>
        <p>
          If you do not agree with all of the following Terms of Use, please do
          not use this Website.
        </p>
        <p>
          <h3>
            1. Use of Our Website and of Materials Contained on this Website
          </h3>
        </p>
        <p>
          The Website contains material that is derived in whole or in part from
          material supplied by Life Protect 24/7 and other sources, and is
          protected by U.S. and international copyright and trademark laws. You
          may only view, use and download material on this Website for your
          non-commercial, personal use, subject to these Terms of Use. This
          limited, non-exclusive, revocable right is provided to you for
          personal purposes only; if you desire to use any material or content
          from this Website for any commercial or other purposes, you must
          obtain our prior written permission. You may not frame this site nor
          link to a page other than the home page without our express permission
          in writing. You may not modify, copy, distribute, transmit, display,
          perform, reproduce, publish, license, create derivative works from,
          transfer, or sell any information, products or services obtained from
          our Website or through use of our services. The information contained
          in this Website is intended for general information purposes only. We
          have made reasonable efforts to ensure that the information on this
          Website is accurate at the time of posting; however, there may be
          inaccuracies and occasional errors. We make no representations or
          warranties about the information provided on or through this Website,
          including but not limited to information obtained through links to any
          third-party websites, and we do not assume any responsibility for
          updating information provided on or through this Website. We accept no
          liability for any inaccuracies or omissions in this Website and any
          decisions based on information contained on this Website are the sole
          responsibility of the user.
        </p>
        <p>You agree not to use our Website or services to:</p>
        <ul>
          <li>
            Promote surveys, contests, pyramid schemes, chain letters, junk
            email, spamming or any duplicative or unsolicited messages
            (commercial or otherwise).
          </li>
          <li>
            Defame, abuse, harass, stalk, threaten or otherwise violate the
            legal rights (such as rights of privacy and publicity) of others.
          </li>
          <li>
            Publish, post, upload, distribute or disseminate any inappropriate,
            profane, defamatory, obscene, indecent or unlawful topic, name,
            material or information.
          </li>
          <li>
            Upload, or otherwise make available, files that contain images,
            photographs, software or other material protected by intellectual
            property laws, including, by way of example, and not as limitation,
            copyright or trademark laws (or by rights of privacy or publicity)
            unless you own or control the rights thereto or have received all
            necessary consent to do the same.
          </li>
          <li>
            Use any material or information, including images or photographs,
            which are made available through the services in any manner that
            infringes any copyright, trademark, patent, trade secret, or other
            proprietary right of any party.
          </li>
          <li>
            Upload files that contain viruses, Trojan horses, worms, time bombs,
            cancelbots, corrupted files, or any other similar software or
            programs that may damage the operation of another’s computer or
            property of another.
          </li>
          <li>
            Restrict or inhibit any other user from using and enjoying our
            Website and services.
          </li>
          <li>
            Harvest or otherwise collect information about others, including
            e-mail addresses.
          </li>
          <li>Violate any applicable laws or regulations.</li>
          <li>Create a false identity for the purpose of misleading others.</li>
          <li>
            Use, download or otherwise copy, or provide (whether or not for a
            fee) to a person or entity any directory of users of the services or
            other user or usage information or any portion thereof.
          </li>
        </ul>
        <p>
          <h3>2. Compliance with Law</h3>
        </p>
        <p>
          You shall not use this Website for any illegal purposes. You agree not
          to send any unsolicited promotional or advertising material, spam or
          similar materials or any volume messages and/or interfere with the
          operation of this Website or with the enjoyment of this Website by
          other users.
        </p>
        <p>
          <h3>3. Links to Third Party Sites3. Links to Third Party Sites</h3>
        </p>
        <p>
          The presence of links to third party sites does not constitute or
          imply endorsement by Life Protect 24/7 of the opinions or views
          expressed by these linked websites, and Life Protect 24/7 does not
          verify, endorse, or take responsibility for the accuracy, currency,
          completeness or quality of the content contained in these sites.
          Furthermore, Life Protect 24/7 is not responsible for the quality or
          delivery of the products or services offered, accessed, obtained by or
          advertised by or through such third party sites. To the extent that
          third party sites collect personally identifiable information from
          you, please be advised that in no event shall Life Protect 24/7 assume
          or have any responsibility or liability for the manner in which such
          information is collected or for any claims, damages, or losses,
          whether in contract, tort (including negligence) or otherwise, arising
          out of or in connection with your access to and/or use of those sites.
          In no event will Life Protect 24/7 be liable to you for any direct,
          indirect, incidental, consequential, or special loss or other damage
          arising out of or in connection with your use of such third party
          site(s).
        </p>
        <p>
          <h3>4. Termination of the Website and/or Access Thereto</h3>
        </p>
        <p>
          Life Protect 24/7 reserves the right, at any time and without notice,
          to modify, alter, suspend or terminate all or any part of this Website
          and to restrict or prohibit access to it. Life Protect 24/7 shall not
          be liable to any party for such modification, alteration, suspension,
          restriction or termination.
        </p>
        <p>
          If you object to any of the Terms of Use contained herein, you may
          discontinue use of the Website. Sections 1 through 3 and 5 through 13
          will survive any termination of your access to the Website, whether we
          terminate your access or you voluntarily discontinue your use.
        </p>
        <p>
          <h3>5. Indemnification</h3>
        </p>
        <p>
          You agree to indemnify, defend and hold harmless, Life Protect 24/7,
          its officers, directors, managers, members, owners, employees, agents,
          licensors, representatives, affiliates, successors, assigns, and third
          party providers to the Website from and against all losses, expenses,
          damages, costs, liabilities, deficiencies, claims, actions, judgments,
          settlements, interest, awards, penalties, or fines of whatever kind,
          including reasonable attorneys’ fees, arising from or relating to your
          use or misuse of the&nbsp; Website or violation of these Terms of Use
          by you.&nbsp; Furthermore, you agree that we assume no responsibility
          for the content you submit or make available through the Website
          except as set forth in our Privacy Policy.
        </p>
        <p>
          <h3>6. Responsibility for Fees and Usage Charges</h3>
        </p>
        <p>
          By submitting your email address and/or cell phone number through the
          Website, you acknowledge that you may receive e-mail or text messages
          on your phone or mobile communications device, and that the receipt of
          such messages may cause you to incur usage charges or other fees or
          costs in accordance with your wireless or data service plan. Any and
          all such charges, fees, or costs are your sole responsibility. You
          should consult with your wireless carrier to determine what rates,
          charges, fees, or costs may apply to your use of Website content.
        </p>
        <p>
          <h3>7. Limitation of Liability/Disclaimer</h3>
        </p>
        <p>
          Your use and browsing of the Website is at your own risk. If you are
          dissatisfied with any of the Materials contained in the Website, or
          with any of these Terms of Use, your sole and exclusive remedy is to
          discontinue accessing and using the Website. To the fullest extent
          permissible pursuant to applicable law, neither Life Protect 24/7 nor
          any other party involved in creating, producing or delivering material
          to the site shall assume or have any responsibility or liability for
          any claims, damages, or losses, whether contractual, tort (including
          negligence), statutory, or otherwise, arising out of or in connection
          with your access to, use of, and/or inability to access the Website.
          Without limiting the foregoing, everything on the Website is provided
          to you “as is” without warranty of any kind, either expressed or
          implied, including, but not limited to, the implied warranties of
          merchantability, fitness for a particular purpose, or
          non-infringement. Life Protect 24/7 also assumes no responsibility,
          and shall not be liable for any damages to, or viruses that may
          infect, your computer equipment or other property on account of your
          access to, use of, or browsing in the Website or your downloading of
          any content or materials from the Website.
        </p>
        <p>
          <h3>
            8. Notice and Procedure for Making Claims of Copyright Infringement
          </h3>
        </p>
        <p>
          If you believe that material posted on the Website infringes your
          copyrights, please provide our copyright agent the following
          information required by the Online Copyright Infringement Liability
          Limitation Act of the Digital Millennium Copyright Act (“DMCA”): (a) a
          physical or electronic signature of a person authorized to act on
          behalf of the owner of an exclusive right that is allegedly infringed;
          (b) identification of the copyright work claimed to have been
          infringed, or, if multiple copyrighted works at a single online site
          are covered by a single notification, a representative list of such
          works at that site; (c) identification of the material that is claimed
          to be infringing or to be the subject of infringing activity and
          information reasonably sufficient to permit us to locate the material;
          (d) information reasonably sufficient to permit us to contact the
          complaining party; (e) a statement that the complaining party has a
          good-faith belief that use of the material in the manner complained of
          is not authorized by the copyright owner, its agent, or the law; and
          (f) a statement that the information in the notification is accurate,
          and under penalty of perjury, that the complaining party is authorized
          to act on behalf of the owner of an exclusive right that is allegedly
          infringed.
        </p>
        <p>
          Our copyright agent for notice of claims of copyright infringement on
          or regarding the Website can be reached as follows:
        </p>
        <p>
          Service Provider: Life Protect 24/7, Inc. Registered Agent/Registered
          Office: Mark E. Slaughter 222 Central Park Ave., Suite 1500 Virginia
          Beach Va23462 (757) 628-5660 Mslaughter@wilsav.com
        </p>
        <p>
          Upon receipt of a Notification, service provider will take such action
          as appropriate under the DMCA.
        </p>
        <p>
          <h3>9. Privacy and Protection of Personal Information</h3>
        </p>
        <p>
          The collection and use of your personal information is subject to the
          terms and conditions set forth in our Privacy Policy available at{" "}
          <a href="/privacy-policy">
            https://lifeprotect247.com/privacy-policy
          </a>
          .
        </p>
        <p>
          <h3>10. Governing Law</h3>
        </p>
        <p>
          These Terms of Use shall be governed by the laws of the State of
          Virginia without regard to its conflicts of laws rules.
        </p>
        <p>
          You agree that jurisdiction over and venue in any legal proceeding
          directly or indirectly arising out of or relating to the Website, your
          use and access thereof, and these Terms of Use will be solely in the
          state courts of competent jurisdiction located in Virginia Beach,
          Virginia or the United States District Court for the Eastern District
          of Virginia, Norfolk Division, located in Norfolk, Virginia, as
          appropriate, and you hereby consent and submit to the exclusive
          personal jurisdiction and venue of the foregoing courts for any such
          legal proceeding.
        </p>
        <p>
          You agree that regardless of any statute or law to the contrary, any
          claim or cause of action arising out of or related to your use of our
          Website or these Terms of Use must be filed within one (1) year after
          such claim or cause of action accrues or be forever barred.
        </p>
        <p>
          <h3>11. Dispute Resolution</h3>
        </p>
        <p>
          <span>
            ARBITRATION PROVISION AND JURY TRIAL AND CLASS ACTION WAIVER
          </span>
        </p>
        <p>
          PLEASE READ THIS CAREFULLY. IT AFFECTS YOUR RIGHTS. YOU AGREE THAT BY
          USING THIS WEBSITE, YOU AND LIFE PROTECT 24/7 ARE EACH WAIVING THE
          RIGHT TO A COURT OR JURY TRIAL OR TO PARTICIPATE IN A CLASS ACTION.
          YOU AND LIFE PROTECT 24/7 AGREE THAT EACH MAY BRING CLAIMS AGAINST THE
          OTHER ONLY IN YOUR OR ITS INDIVIDUAL CAPACITY, AND NOT AS A PLAINTIFF
          OR CLASS MEMBER IN ANY PURPORTED CLASS, REPRESENTATIVE, OR COLLECTIVE
          ACTION OR PROCEEDING. ANY ARBITRATION WILL TAKE PLACE ON AN INDIVIDUAL
          BASIS; CLASS ARBITRATIONS AND CLASS ACTIONS ARE NOT PERMITTED.
        </p>
        <p>
          You and Life Protect 24/7 agree that any and all claims and disputes
          arising from or relating in any way to the subject matter of these
          Terms of Use, your use of the Website, or your and the Life Protect
          24/7’s dealings with one another in connection with this Website shall
          be finally settled and resolved through BINDING INDIVIDUAL ARBITRATION
          as described in this section. This agreement to arbitrate is intended
          to be interpreted broadly. The arbitration will be governed by the
          Commercial Arbitration Rules and the Supplementary Procedures for
          Consumer Related Disputes of the American Arbitration Association
          (“AAA”), as modified by this section. The arbitration will be
          conducted by Judicial Arbiter Group, Inc. (“JAG”) using one arbitrator
          with substantial experience in resolving commercial contract disputes,
          who shall be selected from the appropriate list of JAG arbitrators in
          accordance with the Arbitration Rules and Procedures of JAG. If JAG is
          unable or unwilling to arbitrate a dispute, then the dispute may be
          referred to any other arbitration organization or arbitrator that you
          and Life Protect 24/7 both agree upon in writing or that is appointed
          pursuant to section 5 of the Federal Arbitration Act. For any claim
          where the total amount of the award sought is $10,000 or less, the
          arbitrator, you, and Life Protect 24/7 must abide by the following
          rules: (a) the arbitration shall be conducted solely based on
          telephone or online appearances and/or written submissions; and (b)
          the arbitration shall not involve any personal appearance by the
          parties or witnesses unless otherwise mutually agreed by the parties.
          If the claim exceeds $10,000, the right to a hearing will be
          determined by the AAA rules, and the hearing (if any) must take place
          in Virginia Beach, Virginia. The arbitrator’s ruling is binding and
          may be entered as a judgment in any court of competent jurisdiction,
          or application may be made to such court for judicial acceptance of
          any award and an order of enforcement, as the case may be.
        </p>
        <p>
          There is no judge or jury in arbitration. Arbitration procedures are
          simpler and more limited than rules applicable in court, and review by
          a court is limited. Neither you nor Life Protect 24/7 will be able to
          have a court or jury trial or participate in a class action or class
          arbitration. You and the Life Protect 24/7 each understand and agree
          that by agreeing to resolve any dispute through individual
          arbitration, YOU AND LIFE PROTECT 24/7 ARE EACH WAIVING THE RIGHT TO A
          COURT OR JURY TRIAL. ANY DISPUTE SHALL BE ARBITRATED ON AN INDIVIDUAL
          BASIS, AND NOT AS A CLASS ACTION, REPRESENTATIVE ACTION, CLASS
          ARBITRATION, OR ANY SIMILAR PROCEEDING. The arbitrator may not
          consolidate the claims of multiple parties.
        </p>
        <p>
          ANY CAUSE OF ACTION OR CLAIM YOU MAY HAVE ARISING OUT OF OR RELATING
          IN ANY WAY TO THESE TERMS OF USE, YOUR USE OF THE WEBSITE, OR YOUR AND
          LIFE PROTECT 24/7’S DEALINGS WITH ONE ANOTHER IN CONNECTION WITH THE
          WEBSITE MUST BE COMMENCED IN ARBITRATION WITHIN ONE (1) YEAR AFTER THE
          CAUSE OF ACTION ACCRUES. AFTER THAT ONE (1)-YEAR PERIOD, SUCH CAUSE OF
          ACTION OR CLAIM IS PERMANENTLY BARRED.
        </p>
        <p>
          You and we agree that all challenges to the validity and applicability
          of the arbitration provision—i.e. whether a particular claim or
          dispute is subject to arbitration—shall be determined by the
          arbitrator. Notwithstanding any provision in these terms to the
          contrary, if the class-action waiver above is deemed invalid or
          unenforceable, neither you nor we are entitled to arbitration. If the
          arbitration provision in this section is found unenforceable or to not
          apply for a given dispute, then the proceeding must be brought
          exclusively in the state courts of competent jurisdiction located in
          Virginia Beach, Virginia or the United States District Court for the
          Eastern District of Virginia, Norfolk Division, located in Norfolk,
          Virginia, as appropriate, and you agree to submit to the personal
          jurisdiction of each of these courts for the purpose of litigating
          such claims or disputes, and you still waive your right to a jury
          trial, waive your right to initiate or proceed in a class or
          collective action, and remain bound by any and all limitations on
          liability and damages included in these Terms of Use. This arbitration
          agreement will survive termination of your use of this Website. This
          arbitration agreement involves interstate commerce and, therefore,
          shall be governed by the Federal Arbitration Act, 9 U.S.C. §§ 1-16
          (“FAA”), and not by state law. Information on AAA and how to start
          arbitration can be found at www.adr.org or by calling 800-778-7879. If
          you wish to initiate arbitration against Life Protect 24/7, you must
          send notice to Life Protect 24/7 to the address below.
        </p>
        <p>
          If you wish to opt-out of the agreement to arbitrate, within 30
          (thirty) days of when you first use the Website or submit through the
          Website a request for information, you must send Life Protect 24/7 a
          letter stating “Request to Opt-Out of Agreement to Arbitrate” at the
          following address:
        </p>
        <p>
          Life Protect 24/7, Inc. 3509 Virginia Beach Blvd. Virginia Beach,
          Virginia 23452 Attn: Legal Department
        </p>
        <p>
          In the event you opt out of the arbitration provision, you agree to
          litigate exclusively in the state courts of competent jurisdiction
          located in Virginia Beach, Virginia or the United States District
          Court for the Eastern District of Virginia, Norfolk Division, located
          in Norfolk, Virginia, as appropriate, and you agree to submit to the
          personal jurisdiction of each of these courts for the purpose of
          litigating such claims or disputes, and you still waive your right to
          a jury trial, waive your right to initiate or proceed in a class or
          collective action, and remain bound by any and all limitations on
          liability and damages included in these Terms of Use.
        </p>
        <p>
          <h3>12. Waivers</h3>
        </p>
        <p>
          <span>
            <h3>CALIFORNIA WAIVER.</h3>
          </span>
        </p>
        <p>
          <span>
            <h3>
              IF YOU ARE A CALIFORNIA RESIDENT, YOU WAIVE CALIFORNIA CIVIL CODE
              1542, WHICH SAYS:&nbsp; “A GENERAL RELEASE DOES NOT EXTEND TO
              CLAIMS WHICH THE CREDITOR DOES NOT KNOW OR SUSPECT TO EXIST IN HIS
              FAVOR AT THE TIME OF EXECUTING THE RELEASE, WHICH IF KNOWN BY HIM
              MUST HAVE MATERIALLY AFFECTED HIS SETTLEMENT WITH THE DEBTOR.”
            </h3>
          </span>
        </p>
        <p>
          <span>
            <h3>NEW JERSEY WAIVER.</h3>
          </span>
        </p>
        <p>
          <span>
            <h3>
              IF YOU ARE ACCESSING THE SITE FROM NEW JERSEY, YOU (I) ASSUME ALL
              RISKS OF LOSSES OR DAMAGES RESULTING FROM YOUR USE OF OR INABILITY
              TO USE THE SITE; (II) IRREVOCABLY WAIVE ALL LOSSES OR INDIRECT,
              SPECIAL, CONSEQUENTIAL, PUNITIVE OR INCIDENTAL DAMAGES (INCLUDING,
              WITHOUT LIMITATION, THOSE RESULTING FROM ANY MEDICAL ISSUE) THAT
              MAY OCCUR AS A RESULT OF YOUR USE OF THE SITE; AND (III) EXPRESSLY
              AGREE TO RELEASE AND DISCHARGE US , AND OUR AFFILIATES, EMPLOYEES,
              AGENTS, REPRESENTATIVES, SUCCESSORS, OR ASSIGNS FROM ANY AND ALL
              CLAIMS OR CAUSES OF ACTION RESULTING, DIRECTLY OR INDIRECTLY, FROM
              YOUR USE OF THE SITE;&nbsp; AND (IV) YOU VOLUNTARILY GIVE UP OR
              WAIVE ANY RIGHT THAT YOU MAY OTHERWISE HAVE TO BRING A LEGAL
              ACTION AGAINST US FOR LOSSES OR DAMAGES, WHETHER BASED ON
              WARRANTY, CONTRACT, TORT OR OTHER LEGAL THEORY, INCLUDING ANY
              CLAIM BASED ON ALLEGED NEGLIGENCE ON OUR PART OR OUR AGENTS AND
              EMPLOYEES. YOU ACKNOWLEDGE THAT YOU HAVE CAREFULLY READ THIS
              SECTION AND FULLY UNDERSTAND THAT IT IS A RELEASE OF LIABILITY.
            </h3>
          </span>
        </p>
        <p>
          <h3>13. General</h3>
        </p>
        <p>
          This site is intended exclusively for residents of the United States
          ages 13 and above.
        </p>
        <p>
          We may be required by state or federal law to notify you of certain
          events.&nbsp; You hereby acknowledge and agree that such notices will
          be effective upon our posting them on our Website or delivering them
          to you via email.&nbsp; You may update your email address by visiting
          the area of the Website where you have provided contact
          information.&nbsp; If you do not provide us with accurate information,
          we will not be responsible for failure to notify you.&nbsp; These
          Terms of Use constitute the entire agreement between you and us in
          regards to your access and use of the Website and supersede all prior
          agreements with respect to the subject matter hereof.
        </p>
        <p>
          <em>
            Our failure to exercise or enforce any right or provision of these
            Terms of Use shall not constitute a waiver of such right or
            provision.
          </em>
        </p>
        <p>
          These Terms of Use contain the entire understanding and supersede all
          prior understanding of the parties hereto relating to the subject
          matter hereof, and cannot be changed or terminated orally. If any
          provision of these Terms of Use are found to be illegal or
          unenforceable, these Terms of Use will be deemed curtailed to the
          extent necessary to make them legal and enforceable and will remain,
          as modified, in full force and effect.
        </p>
        <p>
          <h3>14. Location-Based Services</h3>
        </p>
        <p>
          You authorize Life Protect 24/7 to collect your IoT emergency response
          pendant device location from third parties including wireless carries
          and to use it to provide assistance services to you. Life Protect 24/7
          will share your location with individuals you authorize as well as
          first responders and call centers in order to provide you with
          assistance, including showing your location in near-real time in a web
          portal or application.
        </p>
        <p>
          Your consent will be valid for the duration of your relationship with
          us unless you revoke it. You may revoke your consent by
          calling&nbsp;(844) 203-5617. If you do not consent or you revoke
          consent, we may be unable to provide you with assistance.
        </p>
        <p>
          Please see our privacy policy at&nbsp;
          <a href="https://lifeprotect247.com/privacy-policy">
            https://lifeprotect247.com/privacy-policy
          </a>
          for information about how we store and protect your data.
        </p>
      </div>
      <style jsx>{`
        .terms {
          margin: 30px 0;
        }
        .terms h3 {
          margin: 15px 0;
        }
        .terms p {
          margin: 0px 20px;
          line-height: 30px;
        }
        ul {
          list-style: circle inside;
          margin-left: 30px;
        }
      `}</style>
    </>
  );
}
