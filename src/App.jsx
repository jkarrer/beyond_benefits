import Header from "../componets/Header";
import Title from "../componets/Title";
import Timer from "../componets/Timer";
import Content from "../componets/Content";
import Button from "../componets/Button";
import Footer from "../componets/Footer";

export default function Home() {
  return (
    <>
      <div>
        <Header />
        <main>
          <Title />
          <Timer />
          <Content />
          <Button />
        </main>
        <Footer />
      </div>
    </>
  );
}
