import Header from "../componets/Header";

export default function Privacy() {
  return (
    <>
      <Header />
      <div class="privacy">
        <p>
          <h3>Privacy Policy</h3>
        </p>
        <p>
          This privacy policy has been compiled to better serve those who are
          concerned with how their personally identifiable information (PII) is
          being used online. PII, as used in US privacy law and information
          security, is information that can be used on its own or with other
          information to identify, contact, or locate a single person, or to
          identify an individual in context. Please read our privacy policy
          carefully to get a clear understanding of how we collect, use, protect
          or otherwise handle your personally identifiable information in
          accordance with our website.
        </p>
        <p></p>
        <p>
          <h3>
            What personal information do we collect from the people that visit
            our website or app?
          </h3>
        </p>
        <p></p>
        <p>
          When ordering or registering on our site, as appropriate, you may be
          asked to enter your name, email address, mailing address, phone
          number, credit card information, social security number or other
          details to help you with your experience. Additionally, we passively
          collect information about our visitors using cookies, as set out
          below.
        </p>
        <p></p>
        <p>
          <h3>When do we collect information?</h3>
        </p>
        <p></p>
        <p>
          We collect information from you when you register on our site, place
          an order, subscribe to a newsletter, respond to a survey, fill out a
          form, Use Live Chat, Open a Support Ticket or enter information on our
          site.
        </p>
        <p></p>
        <p>
          <h3>How do we use your information?</h3>
        </p>
        <p></p>
        <p>
          We may use the information we collect from you when you register, make
          a purchase, sign up for our newsletter, respond to a survey or
          marketing communication, surf the website, or use certain other site
          features in the following ways:
        </p>
        <p></p>
        <ul>
          <li>
            To personalize user’s experience and to allow us to deliver the type
            of content and product offerings in which you are most interested.
          </li>
          <li>To improve our website in order to better serve you.</li>
          <li>
            To allow us to better service you in responding to your customer
            service requests.
          </li>
          <li>
            To administer a contest, promotion, survey or other site feature.
          </li>
          <li>To quickly process your transactions.</li>
          <li>To ask for ratings and reviews of services or products</li>
          <li>
            To follow up with them after correspondence (live chat, email or
            phone inquiries)
          </li>
        </ul>
        <p></p>
        <p>
          <h3>How do we protect visitor information?</h3>
        </p>
        <p></p>
        <p>
          Our website is scanned on a regular basis for security holes and known
          vulnerabilities in order to make your visit to our site as safe as
          possible.
        </p>
        <p></p>
        <p>We use regular Malware Scanning.</p>
        <p></p>
        <p>
          Your personal information is contained behind secured networks and is
          only accessible by a limited number of persons who have special access
          rights to such systems, and are required to keep the information
          confidential. In addition, all sensitive/credit information you supply
          is encrypted via Secure Socket Layer (SSL) technology.
        </p>
        <p></p>
        <p>
          We implement a variety of security measures when a user places an
          order to maintain the safety of your personal information.
        </p>
        <p></p>
        <p>
          For your convenience, we may store your credit card information kept
          for more than 60 days in order to expedite future orders, and to
          automate the billing process.
        </p>
        <p></p>
        <p>
          <h3>Do we use ‘cookies’?</h3>
        </p>
        <p></p>
        <p>
          Yes. Cookies are small files that a site or its service provider
          transfers to your computer’s hard drive through your Web browser (if
          you allow) that enables the site’s or service provider’s systems to
          recognize your browser and capture and remember certain information.
          For instance, we use cookies to help us remember and process the items
          in your shopping cart. They are also used to help us understand your
          preferences based on previous or current site activity, which enables
          us to provide you with improved services. We also use cookies to help
          us compile aggregate data about site traffic and site interaction so
          that we can offer better site experiences and tools in the future.
        </p>
        <p></p>
        <p>We use cookies to:</p>
        <p></p>
        <ul>
          <li>Help remember and process the items in the shopping cart.</li>
          <li>Understand and save user’s preferences for future visits.</li>
          <li>
            Compile aggregate data about site traffic and site interactions in
            order to offer better site experiences and tools in the future. We
            may also use trusted third-party services that track this
            information on our behalf.
          </li>
        </ul>
        <p></p>
        <p>
          You can choose to have your computer warn you each time a cookie is
          being sent, or you can choose to turn off all cookies. You do this
          through your browser (like Internet Explorer or Google Chrome)
          settings. Each browser is a little different, so look at your
          browser’s Help menu to learn the correct way to modify your cookies.
        </p>
        <p></p>
        <p>
          <h3>If users disable cookies in their browser:</h3>
        </p>
        <p></p>
        <p>
          If you disable cookies off, some features will be disabled It will
          turn off some of the features that make your site experience more
          efficient and some of our services will not function properly.
        </p>
        <p></p>
        <p>
          However, you can still place orders over the telephone by contacting
          customer service.
        </p>
        <p></p>

        <p>
          Non-personally identifiable visitor information may be provided to
          other parties for marketing, advertising, or other uses.
        </p>
        <p></p>
        <p>
          <h3>Third-party links</h3>
        </p>
        <p></p>
        <p>
          Occasionally, at our discretion, we may include or offer third-party
          products or services on our website. These third-parties have separate
          and independent privacy policies. We, therefore, have no
          responsibility or liability for the content and activities of these
          linked sites. Nonetheless, we seek to protect the integrity of our
          site and welcome any feedback about these sites.
        </p>
        <p></p>

        <p>
          <h3>COPPA (Children Online Privacy Protection Act)</h3>
        </p>
        <p></p>
        <p>
          We do not specifically market to children under 13, and do not
          knowingly collect personal information from children under 13. If you
          believe we have collected information regarding a child under 13 in
          error, please contact us below.
        </p>
        <p></p>
      </div>
      <style jsx>{`
        .privacy {
          margin: 30px 0;
        }
        .privacy h3 {
          margin: 15px 0;
        }
        .privacy p {
          margin: 0px 20px;
          line-height: 30px;
        }
        ul {
          list-style: circle inside;
          margin-left: 30px;
        }
      `}</style>
    </>
  );
}
