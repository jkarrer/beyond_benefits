import React from "react";
import "./index.css";
import ReactDOM from "react-dom/client";
import {
  createBrowserRouter,
  RouterProvider,
  Navigate,
} from "react-router-dom";
import App from "./App";
import Privacy from "./Privacy";
import Terms from "./Terms";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Navigate to="/medical-alert" replace={true} />,
  },

  {
    path: "/medical-alert",
    element: <App />,
  },
  {
    path: "/privacy",
    element: <Privacy />,
  },
  {
    path: "/terms",
    element: <Terms />,
  },
]);

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);
