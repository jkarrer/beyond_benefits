export class TeleforceTracker {
  constructor(campaignId) {
    this.campaignId = campaignId;
    this.clientData = {
      queries: {},
      cookies: {},
    };
  }

  async requestNumber() {
    this.assignQueryParameters();
    this.assignUserCookies();

    console.log(this.clientData);
    // const request = await fetch("https://ed56-12-247-105-170.ngrok.io", {
    //   method: "POST",
    //   headers: {
    //     "Content-Type": "application/json",
    //   },
    //   body: JSON.stringify({
    //     client_data: this.clientData,
    //     campaign_id: this.campaignId,
    //   }),
    // });

    return "1-800-000-0000";
  }

  assignUserCookies() {
    let decodedCookie = decodeURIComponent(window.document.cookie);
    let sessionCookies = decodedCookie.split(";");

    for (let i = 0; i < sessionCookies.length; i++) {
      this.clientData.cookies[i] = sessionCookies[i];
    }
  }

  assignQueryParameters() {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);

    for (const [key, value] of urlParams.entries()) {
      this.clientData.queries[key] = value;
    }
  }
}
// const Tracker = new TeleforceTracker("camp_id_888");
